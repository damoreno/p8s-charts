{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "p8s-puppetserver.name" -}}
{{- default .Chart.Name .Values.overrides.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "p8s-puppetserver.fullname" -}}
{{- if .Values.overrides.fullname -}}
{{- .Values.overrides.fullname | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.overrides.name -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "p8s-puppetserver.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Generates Java arguments for puppetserver.
*/}}
{{- define "p8s-puppetserver.javaArgs" -}}
{{- $memory := .Values.resources.requests.memory | replace "Mi" "" -}}
{{- printf "-Xms%sm -Xmx%sm -XX:MaxPermSize=256m" $memory $memory -}}
{{- end -}}