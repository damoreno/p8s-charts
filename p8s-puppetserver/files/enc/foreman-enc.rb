#!/opt/puppetlabs/puppet/bin/ruby

require 'optparse'
require 'rubygems'
require 'yaml'
require 'json'
require 'net/http'
require 'net/https'
require 'fileutils'
require 'timeout'

SETTINGS = {
  :url           => 'https://judy.cern.ch:8007',
  :puppetdir     => '/opt/puppetlabs/server/data/puppetserver',
  :puppetlogdir  => '/var/log/puppetlabs/puppetserver',
  :facts         => true,
  :cachefallback => true,
  :timeout       => 5,
  :factstimeout  => 8,
  :storeconfigs  => false,
  # if CA is specified, remote Foreman host will be verified
  :ssl_ca        => {{ .Values.puppetserver.ssl.ca | quote }},
  # ssl_cert and key are required if require_ssl_puppetmasters is enabled in Foreman
  :ssl_cert      => {{ .Values.puppetserver.ssl.cert | quote }},
  :ssl_key       => {{ .Values.puppetserver.ssl.key | quote }},
  :max_enc_attempts => 2
}

OptionParser.new do |opts|
  opts.banner = "Usage: #{$0} [options]"

  opts.on('--[no-]facts', 'Upload facts to foreman.') do |f|
    SETTINGS[:facts] = f
  end

  opts.on('--no-cache-fallback', "Don't use cached values for the ENC return data.") do
    SETTINGS[:cachefallback] = false
  end

  opts.on('--url URL', String, 'foreman URL') do |u|
    SETTINGS[:url] = u
  end

  opts.on('--puppetdir DIR', String, 'puppet base directory') do |d|
    SETTINGS[:puppetdir] = d
  end

  opts.on('--timeout SECS', Integer, 'timeout for contacting foreman') do |t|
    raise 'Timeout must be a non-negative integer.' unless t > 0
    SETTINGS[:timeout] = t
  end
end.parse!

### Do not edit below this line

class ENCError < StandardError
end

def url
  SETTINGS[:url] || raise("Must provide foreman URL")
end

def certname
  ARGV[0] || raise("Must provide certname as an argument")
end

def puppetdir
  SETTINGS[:puppetdir] || raise("Must provide puppet base directory")
end

def puppetlogdir
  SETTINGS[:puppetlogdir] || raise("Must provide puppet base directory")
end

def stat_file
  FileUtils.mkdir_p "#{puppetdir}/yaml/foreman/"
  "#{puppetdir}/yaml/foreman/#{certname}.yaml"
end

def tsecs
  SETTINGS[:timeout] || 3
end

def ftsecs
  SETTINGS[:factstimeout] || 8
end

def build_body(certname,filename)
  # Strip the Puppet:: ruby objects and keep the plain hash
  facts        = File.read(filename)
  puppet_facts = YAML::load(facts.gsub(/\!ruby\/object.*$/,''))
  hostname     = puppet_facts['values']['fqdn'] || certname
  if puppet_facts.key?('timestamp')
    puppet_facts['values']['_timestamp'] = puppet_facts['timestamp']
  end
  {'facts' => puppet_facts['values'], 'name' => hostname, 'certname' => certname}
end

def upload_facts
  # Temp file keeping the last run time
  loginfo "Starting facts call for #{certname}"
  last_run = File.exists?(stat_file) ? File.stat(stat_file).mtime.utc : Time.now - 365*24*60*60
  filename = "#{puppetdir}/yaml/facts/#{certname}.yaml"
  if not File.exists?(filename)
    loginfo "Facts for #{certname} not uploaded: fact file not found"
    return
  end
  last_fact = File.stat(filename).mtime.utc
  if last_fact > last_run
    begin
      uri = URI.parse("#{url}/api/hosts/facts")
      req = Net::HTTP::Post.new(uri.request_uri)
      req.add_field('Accept', 'application/json,version=2' )
      req.content_type = 'application/json'
      req.body         = build_body(certname, filename).to_json
      res             = Net::HTTP.new(uri.host, uri.port)
      res.use_ssl     = uri.scheme == 'https'
      res.read_timeout = ftsecs
      if res.use_ssl?
        if SETTINGS[:ssl_ca]
          res.ca_file = SETTINGS[:ssl_ca]
          res.verify_mode = OpenSSL::SSL::VERIFY_PEER
        else
          res.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        if SETTINGS[:ssl_cert] and SETTINGS[:ssl_key]
          res.cert = OpenSSL::X509::Certificate.new(File.read(SETTINGS[:ssl_cert]))
          res.key  = OpenSSL::PKey::RSA.new(File.read(SETTINGS[:ssl_key]), nil)
        end
      end
      res.start { |http| http.request(req) }
      loginfo "Successfully uploaded facts for #{certname}"
    rescue => error
      logerror "Failed to upload facts for #{certname} (#{error})"
    end
  else
      loginfo "Facts for #{certname} not uploaded: #{last_fact} <= #{last_run}"
  end
end

def cache result
  begin
    File.open(stat_file, 'w') {|f| f.write(result) }
  rescue => error
    logerror "Failed to cache ENC data call for #{certname} (#{error})"
  end
end

def read_cache
  begin
    File.read(stat_file)
  rescue => error
    raise ENCError.new("Node #{certname} not found in cache (#{error})")
  end
end

def enc
  foreman_url      = "#{url}/node/#{certname}?format=yml"
  uri              = URI.parse(foreman_url)
  req              = Net::HTTP::Get.new(foreman_url)
  http             = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl     = uri.scheme == 'https'
  http.read_timeout = tsecs
  if http.use_ssl?
    if SETTINGS[:ssl_ca]
      http.ca_file = SETTINGS[:ssl_ca]
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    else
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    if SETTINGS[:ssl_cert] and SETTINGS[:ssl_key]
      http.cert = OpenSSL::X509::Certificate.new(File.read(SETTINGS[:ssl_cert]))
      http.key  = OpenSSL::PKey::RSA.new(File.read(SETTINGS[:ssl_key]), nil)
    end
  end

  attempts = 0
  begin
    loginfo "Starting ENC call for #{certname} (att: #{attempts})"
    res = http.start { |http| http.request(req) }
    raise ENCError.new("Node #{certname} not found") if res.code == "404"
    raise ENCError.new("Error retrieving node #{certname}: (#{res.code})") unless res.code == "200"
    loginfo "Successfully retrieved ENC payload for #{certname}"
    res.body
  rescue Timeout::Error, SocketError, Errno::EHOSTUNREACH => error
    logerror "ENC call for #{certname} failed (#{error})"
    attempts = attempts + 1
    retry if(attempts < SETTINGS[:max_enc_attempts])
    # Make the catalog compilation fail if the ENC is down (see AI-2102)
    raise ENCError.new("ENC not responding and cache fallback is off") if not SETTINGS[:cachefallback]
    result = read_cache
  end
end

def validate_toplevel enc
  begin
    data = YAML.load(enc)
    if data["parameters"].key?("hostgroup")
      hostgroup = data["parameters"]["hostgroup"]
      if hostgroup && hostgroup.split("/")[0] != {{ .Values.toplevel | quote }}
        logerror "ENC call for #{certname} from invalid hostgroup (exitcode: 3)"
        puts "Node #{certname} can't request catalogs to {{ .Values.toplevel }}. Please, refer to configdocs.cern.ch/troubleshooting"
        exit 3
      end
    end
  rescue SyntaxError => error
     logerror "ENC call for #{certname} failed (#{error}) (exitcode: 4)"
     puts "The external node classifier (ENC) call failed (#{error})"
     exit 4
  end
end

def log (lvl, message)
  logfile = "#{puppetlogdir}/foreman-enc.log"
  timestamp = Time.now
  msg = "#{timestamp} #{lvl} #{message}\n"
  File.open(logfile, 'a+') {|f| f.write(msg) }
end

def logerror message
  log("ERROR", message)
end

def loginfo message
  log("INFO", message)
end

# Actual code starts here
begin
  loginfo "Script called for #{certname}"
  #
  # query External node
  begin
    result = ""
    result = enc
    #raise ENCError.new("THIS IS A TEST")
  rescue ENCError => error
    logerror "ENC call for #{certname} failed (#{error}) (exitcode: 1)"
    puts "The external node classifier (ENC) call failed (#{error})"
    exit 1
  end

  validate_toplevel(result)

  if SETTINGS[:facts]
    begin
      upload_facts
    rescue Timeout::Error, SocketError, Errno::EHOSTUNREACH => error
      # Silently fail, next run will upload the facts, not critical.
      loginfo "Facts uploading for #{certname} failed (#{error})"
    end
  end

  loginfo "Script finished for #{certname} (exitcode: 0)"
  cache result
  puts result
  exit 0

rescue => error
  logerror "Unexpected exception for #{certname} (#{error}) (exitcode: 2)"
  exit 2
end
